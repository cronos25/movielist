package com.asdev.movielist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.TextView;

import com.asdev.movielist.Connection.GlobalValues;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.asdev.movielist.Connection.GlobalValues.SelectedMovie;
import static com.asdev.movielist.Connection.GlobalValues.rutaLoadImagen;

public class MovieDetail extends AppCompatActivity {

    @BindView(R.id.image_id)
    ImageView backgroundImage;

    @BindView(R.id.imgv_detail_image)
    ImageView imgv_detail_image;

    @BindView(R.id.tv_language)
    TextView tv_language;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_original_title)
    TextView tv_original_title;
    @BindView(R.id.tv_release_date)
    TextView tv_release_date;
    @BindView(R.id.tv_vote_average)
    TextView tv_vote_average;
    @BindView(R.id.movie_description)
    TextView movie_description;

    //variable local
    private ShareActionProvider mShareActionProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        //set activity title to movie title
        try
        {
            getSupportActionBar().setTitle(SelectedMovie.getTitle());  // provide compatibility to all the versions
        }
        catch (Exception ex){
        }
        //load background image
        try {
            Picasso.get().load(rutaLoadImagen+SelectedMovie.getBackdrop_path()).placeholder(R.drawable.progress_load).resize(500,281)
                    .error(R.drawable.ic_broken_image).into(backgroundImage);

        } catch (Exception e){
            e.printStackTrace();
        }


        try {
            Picasso.get().load(rutaLoadImagen+SelectedMovie.getPoster_path()).placeholder(R.drawable.progress_load).resize(500,750)
                    .error(R.drawable.ic_broken_image).into(imgv_detail_image);

        } catch (Exception e){
            e.printStackTrace();
        }
        //set titulo
        try{
            tv_title.setText(Html.fromHtml(getString(R.string.title,SelectedMovie.getTitle())));
        }catch (Exception e){

        }
        //set titulo original
        try{
            tv_original_title.setText(Html.fromHtml(getString(R.string.originalTitle,SelectedMovie.getOriginal_title())));
        }catch (Exception e){

        }

        //set lenguage original
        try{
            tv_language.setText(Html.fromHtml(getString(R.string.originalLanguage,SelectedMovie.getOriginal_language())));
        }catch (Exception e){
        }

        //set fecha estreno
        try{
            tv_release_date.setText(Html.fromHtml(getString(R.string.releaseDate,GlobalValues.formateDateFromstring("yyyy-MM-dd", "dd, MMM yyyy", SelectedMovie.getRelease_date()))));
        }catch (Exception e){

        }

        //set fecha estreno
        try{
            tv_vote_average.setText(Html.fromHtml(getString(R.string.vote,  SelectedMovie.getVote_average(),SelectedMovie.getVote_count())));
        }catch (Exception e){

        }

        //set fecha estreno
        try{
            movie_description.setText(Html.fromHtml(SelectedMovie.getOverview()));
        }catch (Exception e){

        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = getString(R.string.body_share,SelectedMovie.getTitle(),rutaLoadImagen,SelectedMovie.getPoster_path());
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.subject_share));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.title_share)));
            }
        });
    }
}
