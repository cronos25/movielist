package com.asdev.movielist.Connection;

import android.util.Log;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

public class ConnectEngine {
    private static final String BASE_URL = "https://api.themoviedb.org/3/movie/";

    private static final String KEY="34738023d27013e6d1b995443764da44";

    private static AsyncHttpClient client = new AsyncHttpClient();
    private static RequestHandle clientProg;


    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        params.add("api_key",KEY);
        clientProg = client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void stop(){
        try {
            if (clientProg != null){
                clientProg.cancel(true);
            }
        } catch (Exception e){
            e.printStackTrace();
            Log.e("ConnectEngine:48", "stop request error" );
        }

        try{
            //Cancels all pending (or potentially active) requests.
            client.cancelAllRequests(true);
        } catch (Exception e){
            e.printStackTrace();
            Log.e("ConnectEngine:55", "cancel request error" );
        }
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

}
