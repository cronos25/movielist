package com.asdev.movielist.Connection;

import android.util.Log;

import com.asdev.movielist.Class.Movie;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.content.ContentValues.TAG;

/**
 * Created by cronos on 10-11-2018.
 */

public class GlobalValues extends Singleton{

    //ruta global
    public static String rutaLoadImagen = "http://image.tmdb.org/t/p/w500";

    public static ArrayList<Movie> ListaPeliculas = new ArrayList<Movie>();
    public static Movie SelectedMovie;

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (Exception e) {
            Log.e(TAG, "ParseException - dateFormat");
        }

        return outputDate;

    }
}
