package com.asdev.movielist.Connection;

import android.app.Application;
import android.util.Log;

public class Singleton extends Application {


    private static Singleton singleton;

    public static Singleton getInstance(){
        return singleton;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                Log.e("MyApplication", ex.getMessage());

            }
        });
    }

}
