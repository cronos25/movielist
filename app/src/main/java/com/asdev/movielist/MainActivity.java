package com.asdev.movielist;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.asdev.movielist.Class.Movie;
import com.asdev.movielist.Connection.ConnectEngine;
import com.asdev.movielist.Connection.GlobalValues;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

import static com.asdev.movielist.Connection.GlobalValues.ListaPeliculas;
import static com.asdev.movielist.Connection.GlobalValues.rutaLoadImagen;

public class MainActivity extends AppCompatActivity {
    //binding a elementos de la vista
    @BindView(R.id.barra)
    SmoothProgressBar barra;

    @BindView(R.id.gv_listado)
    GridView gv_movies;

    @BindView(R.id.spinner_movie_type_list)
    Spinner spinner_movie_type_list;

    @BindView(R.id.toggleBtnFiltros)
    ToggleButton toggleBtnFiltros;

    @BindView(R.id.content_filtros)
    LinearLayout content_filtros;

    @BindView(R.id.et_movie_name)
    EditText et_movie_name;

    //local variables
    String SearchType = "popular";
    boolean cargando = false;
    CustomGrid adapter;
    int mLastSelected = -1;
    ArrayList<Movie> moviesRespaldo = new ArrayList<Movie>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        GetListaVideos();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
                Intent intent = new Intent(MainActivity.this,webview.class);
                startActivity(intent);

            }
        });

        spinner_movie_type_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner_movie_type_list.getSelectedItem() instanceof String){
                    if (((String)spinner_movie_type_list.getSelectedItem()).equals("Populares")){
                        SearchType = "popular";
                        GetListaVideos();
                    }else{
                        SearchType = "top_rated";
                        GetListaVideos();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        et_movie_name.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filtrar(s);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        ConnectEngine.stop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void filtrar(CharSequence s) {
        ArrayList<Movie> arrayfiltro = new ArrayList<Movie>();
        arrayfiltro.clear();

        int textlength = et_movie_name.getText().length();

        for (int i = 0; i < ListaPeliculas.size(); i++) {
            try{

                if (textlength <= ListaPeliculas.get(i).getTitle().length()) {

                    if (ListaPeliculas.get(i).getTitle().toUpperCase().contains(et_movie_name.getText().toString().toUpperCase()) ||
                            ListaPeliculas.get(i).getOriginal_title().toUpperCase().contains(et_movie_name.getText().toString().toUpperCase())){
                        //  DASGlobal.minimizaDTOsFltro.add(DASGlobal.minimizaDTOs.get(0));
                        arrayfiltro.add(ListaPeliculas.get(i));
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        adapter = new CustomGrid(MainActivity.this, arrayfiltro);
        gv_movies.setAdapter(adapter);
    }


    /********/
    @OnClick(R.id.toggleBtnFiltros)
    public void muestraOcultaFiltros(View v){
        if (toggleBtnFiltros.isChecked()){

            toggleBtnFiltros.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_expand_less),null);
            content_filtros.setVisibility(View.VISIBLE);

        }else{
            toggleBtnFiltros.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_expand_more),null);
            content_filtros.setVisibility(View.GONE);
        }
    }


    public void GetListaVideos(){
        ConnectEngine.get(SearchType, new RequestParams(), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject jo) {
                super.onSuccess(statusCode, headers, jo);
                try {

                    if (statusCode == 200) {

                        try {
                            Gson gson = new Gson();
                            if (jo.get("results") instanceof JSONArray) {
                                ListaPeliculas.clear();
                                ListaPeliculas = new ArrayList<Movie>();
                                JSONArray results = jo.getJSONArray("results");

                                Movie[] MovTemp = gson.fromJson(results.toString(), Movie[].class);
                                for (int i = 0; i < MovTemp.length; i++) {
                                    ListaPeliculas.add(MovTemp[i]);
                                    moviesRespaldo.add(MovTemp[i]);
                                }
                            }

                            barra.setVisibility(View.INVISIBLE);
                                //startActivity(new Intent(aprueba_charlas_main_search.this, aprueba_charlas_lista_videos.class));
                                /**gridview junto a filtros***/

                                adapter = new CustomGrid(MainActivity.this, ListaPeliculas);

                                gv_movies.setAdapter(adapter);
                                /*****/
                        }catch (Exception e){
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject er) {
                super.onFailure(statusCode, headers, throwable, er);
                String message = "Error al ingresar";

            }
        });

    }

    class CustomGrid extends BaseAdapter {
        private Context mContext;
        public ArrayList<Movie> listado;

        public CustomGrid(Context c,ArrayList<Movie> listado2) {
            mContext = c;
            listado = listado2;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return listado.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View grid;
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {

                grid = inflater.inflate(R.layout.movie_item_cell, null);

                if (position%2 == 0){
                    grid.setPaddingRelative(getResources().getDimensionPixelOffset(R.dimen.activity_horizontal_margin),0,0,0);
                }else{
                    grid.setPaddingRelative(0,0,getResources().getDimensionPixelOffset(R.dimen.activity_horizontal_margin),0);
                }
                TextView titleMovie = (TextView) grid.findViewById(R.id.titulo_list);
                titleMovie.setText(listado.get(position).getTitle());

                TextView ranking = (TextView) grid.findViewById(R.id.ranking);
                ranking.setText(getString(R.string.ranking_formated,listado.get(position).getVote_average()));

                TextView releaseDate = (TextView) grid.findViewById(R.id.release_date);
                releaseDate.setText(GlobalValues.formateDateFromstring("yyyy-MM-dd", "dd, MMM yyyy", listado.get(position).getRelease_date()));

                ImageView thumbnail = (ImageView) grid.findViewById(R.id.imgv_thumbnail);
                try {
                    Picasso.get().load(rutaLoadImagen+listado.get(position).getPoster_path()).placeholder(R.drawable.progress_load).resize(500,750).error(R.drawable.ic_broken_image).into(thumbnail);

                } catch (Exception e){
                    e.printStackTrace();
                }

                grid.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!cargando){
                            cargando = true;
                            mLastSelected = position;
                            notifyDataSetChanged();
                            //competencias_play.setEnabled(false);
                            GlobalValues.SelectedMovie = listado.get(position);


                            Intent showImageDetail = new Intent(MainActivity.this,MovieDetail.class);

                            startActivity(showImageDetail);

                            cargando = false;
                        }
                    }
                });

            } else {
                grid = (View) convertView;

                if (position%2 == 0){
                    grid.setPaddingRelative(getResources().getDimensionPixelOffset(R.dimen.activity_horizontal_margin),0,0,0);
                }else{
                    grid.setPaddingRelative(0,0,getResources().getDimensionPixelOffset(R.dimen.activity_horizontal_margin),0);
                }
                TextView titleMovie = (TextView) grid.findViewById(R.id.titulo_list);
                titleMovie.setText(listado.get(position).getTitle());

                TextView ranking = (TextView) grid.findViewById(R.id.ranking);
                ranking.setText(getString(R.string.ranking_formated,listado.get(position).getVote_average()));

                TextView releaseDate = (TextView) grid.findViewById(R.id.release_date);
                releaseDate.setText(GlobalValues.formateDateFromstring("yyyy-MM-dd", "dd, MMM yyyy", listado.get(position).getRelease_date()));

                ImageView thumbnail = (ImageView) grid.findViewById(R.id.imgv_thumbnail);
                try {
                    Picasso.get().load(rutaLoadImagen+listado.get(position).getPoster_path()).placeholder(R.drawable.progress_load).resize(500,750).error(R.drawable.ic_broken_image).into(thumbnail);

                } catch (Exception e){
                    e.printStackTrace();
                }

                grid.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!cargando){
                            cargando = true;
                            mLastSelected = position;
                            notifyDataSetChanged();
                            //competencias_play.setEnabled(false);
                            GlobalValues.SelectedMovie = listado.get(position);

                            Intent showImageDetail = new Intent(MainActivity.this,MovieDetail.class);

                            startActivity(showImageDetail);
                            cargando = false;
                        }
                    }
                });
            }

            return grid;
        }
    }
}
